package com.glint.codechallenge;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;

import org.apache.log4j.Logger;

import com.glint.codechallenge.persistence.Grid;
import com.glint.codechallenge.persistence.IterationState;
import com.glint.codechallenge.services.PersistenceService;
import com.glint.codechallenge.statistics.ProgramStatistics;
import com.glint.codechallenge.util.CellCoordinates;

/**
 * An instance of this class is created for each of the threads that update the data in the grid.
 * @author Akshay More
 *
 */
public class Updater implements Runnable {

	private static Logger logger = Logger.getLogger(Updater.class.getName());
	
	private final String name;
	private final long incrementValue;
	private final Grid grid;
	private Random randomGenerator;
	private PersistenceService persistenceService;
	private ProgramStatistics programStatistics;
	private IterationState iterationState;
	
	public Updater(String name, long incrementValue, Grid grid, Random randomGenerator, PersistenceService persistenceService, ProgramStatistics programStatistics) {
		this.name = name;
		this.incrementValue = incrementValue;
		this.grid = grid;
		this.randomGenerator = randomGenerator;
		this.persistenceService = persistenceService;
		this.programStatistics = programStatistics;
		this.iterationState = persistenceService.getOrCreateIterationStateByGridIdAndUpdaterName(grid.getGridId(), name);
	}
	
	public void run() {
		logger.debug("Starting thread <" + name + ">");
		Collection<CellCoordinates> cellCoordinatesToUpdate = null;
		int completedIterations = iterationState.getCompletedIterations();
		
		int iterationAttempts = 0;
		
		while(completedIterations < grid.getK()) {
			long startNanos = System.nanoTime();
			if (iterationAttempts == 0)
				cellCoordinatesToUpdate = generateCellCoordinatesRandomly();
			iterationAttempts++;
			boolean iterationSucceeded = persistenceService.executeIteration(
					grid.getGridId(), cellCoordinatesToUpdate, incrementValue, iterationState.getIterationStateId());
			programStatistics.addIterationAttemptStatistics(name, completedIterations+1, iterationAttempts, startNanos, System.nanoTime());
			if(iterationSucceeded) {
				completedIterations++;
				iterationAttempts = 0;
			} 
		}
		logger.debug("Completed thread <" + name + ">");
	}

	public int[] randomShuffle(int[] data) {
		for (int i=1; i<data.length; i++) {
			int randomIndex = randomGenerator.nextInt(i+1);
			int temp = data[randomIndex];
			data[randomIndex] = data[i];
			data[i] = temp;
		}
		return data;
	}

	/**
	 * Randomly select M cell coordinates out of NxN cells in the grid.
	 * @return Collection of cell coordinates.
	 */
	private Collection<CellCoordinates> generateCellCoordinatesRandomly() {
		Collection<CellCoordinates> selectedCellCoordinates = new HashSet<CellCoordinates>();
	
		if (grid.getM() < (grid.getN() * grid.getN() / 2)) {
			int[] cellCoordinateNumbers = new int[grid.getN() * grid.getN()];
			for (int i = 0; i < cellCoordinateNumbers.length; i++)
				cellCoordinateNumbers[i] = i;
			cellCoordinateNumbers = randomShuffle(cellCoordinateNumbers);

			for (int i = 0; i < grid.getM(); i++) {
				int selectedCoordinateNumber = cellCoordinateNumbers[i];
				int xCoordOfSelectedCell = selectedCoordinateNumber/grid.getN();
				int yCoordOfSelectedCell = selectedCoordinateNumber % grid.getN();
				selectedCellCoordinates.add(new CellCoordinates(xCoordOfSelectedCell, yCoordOfSelectedCell));
			}
		} else {
			while(selectedCellCoordinates.size() != grid.getM()) {
				int xCoord = randomGenerator.nextInt(grid.getN());
				int yCoord = randomGenerator.nextInt(grid.getN());
				selectedCellCoordinates.add(new CellCoordinates(xCoord, yCoord));
			}
		}
		for (CellCoordinates cellCoords : selectedCellCoordinates)
			logger.trace("Selected cell " + cellCoords.toString() 
				+ " of thread <" + name + ">");

		return selectedCellCoordinates;
	}

}
