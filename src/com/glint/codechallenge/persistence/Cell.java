package com.glint.codechallenge.persistence;

/**
 * An instance of this class represent a cell in the grid.
 * It corresponds to the Cell entity in the database.
 * @author Akshay More
 *
 */
public class Cell {

	private int cellId;

	private long version;
	
	private int gridId;
	private int xCoord;
	private int yCoord;
	private long value;
	
	public Cell() {
	}
	
	public Cell(int gridId, int xCoord, int yCoord, int value) {
		this.gridId = gridId;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.value = value;
	}

	public int getCellId() {
		return cellId;
	}

	public long getVersion() {
		return version;
	}

	public int getGridId() {
		return gridId;
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Cell [cellId=" + cellId + ", version=" + version + ", gridId="
				+ gridId + ", xCoord=" + xCoord + ", yCoord=" + yCoord
				+ ", value=" + value + "]";
	}

	public synchronized void incrementValue(long incrementValue) {
		this.value += incrementValue;
	}
	
	
}
