package com.glint.codechallenge.persistence;

import org.hibernate.*;
import org.hibernate.cfg.*;

/**
 * This is a utility class to help with Hibernate sessions.
 * @author Akshay More
 *
 */
public class HibernateUtil {

	private static SessionFactory sessionFactory;
	
	static {
		try {
			sessionFactory = new Configuration()
					.configure("glint.hibernate.cfg.xml")
					.buildSessionFactory();
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public static void shutdown() {
		getSessionFactory().close();
	}
}
