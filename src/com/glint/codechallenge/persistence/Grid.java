package com.glint.codechallenge.persistence;

import com.glint.codechallenge.exceptions.GridParametersException;

/**
 * An instance of this class represents the grid.
 * It corresponds to the Grid entity in the database.
 * @author Akshay More
 *
 */
public class Grid {

	private int gridId;
	private int N;
	private int K;
	private int M;
	
	public Grid() {
	}

	public Grid(int N, int M, int K) throws GridParametersException {
		if (N < 0 || M < 0 || K < 0)
			throw new GridParametersException("Values of N,K,M should not be negative");
		if (M > (N * N))
			throw new GridParametersException("Value of M should not be greater N^2");
		this.N = N;
		this.M = M;
		this.K = K;
	}

	public int getGridId() {
		return gridId;
	}

	public int getN() {
		return N;
	}

	public int getK() {
		return K;
	}

	public int getM() {
		return M;
	}

	@Override
	public String toString() {
		return "Grid [gridId=" + gridId + ", N=" + N + ", K=" + K + ", M=" + M + "]";
	}
	
	
}
