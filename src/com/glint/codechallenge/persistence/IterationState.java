package com.glint.codechallenge.persistence;

/**
 * An instance of this class represents the state of an updater thread as it progresses through 
 * the iterations.
 * It corresponds to the IterationState entity in the database.
 * @author Akshay More
 *
 */
public class IterationState {

	private int iterationStateId;
	
	private long version;
	
	private int gridId;
	private String updater;
	private int completedIterations;
	
	public IterationState() {
	}

	public IterationState(int gridId, String updater) {
		this.gridId = gridId;
		this.updater = updater;
		this.completedIterations = 0;
	}
	
	public int getIterationStateId() {
		return iterationStateId;
	}

	public long getVersion() {
		return version;
	}

	public int getGridId() {
		return gridId;
	}

	public String getUpdater() {
		return updater;
	}

	public int getCompletedIterations() {
		return completedIterations;
	}

	public void setCompletedIterations(int completedIterations) {
		this.completedIterations = completedIterations;
	}

	@Override
	public String toString() {
		return "[ updater=" + updater 
				+ ", completedIterations=" + completedIterations
				+ ", version=" + version 
				+ "]";
	}

	public synchronized void incrementCompletedIterations() {
		this.completedIterations++;
	}
	
}
