package com.glint.codechallenge.util;

/**
 * An instance of this class represents the X and Y coordinates of a cell in the grid.
 * @author Akshay More
 *
 */
public final class CellCoordinates {

	private final int xCoord;
	private final int yCoord;
	
	public CellCoordinates(int xCoord, int yCoord) {
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}

	public int getxCoord() {
		return xCoord;
	}

	public int getyCoord() {
		return yCoord;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + xCoord;
		result = prime * result + yCoord;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CellCoordinates other = (CellCoordinates) obj;
		if (xCoord != other.xCoord)
			return false;
		if (yCoord != other.yCoord)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CellCoordinates [" + xCoord + ", " + yCoord + "]";
	}
	
	
}
