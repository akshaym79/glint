package com.glint.codechallenge.statistics;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

/**
 * An instance of this class keeps track of the run times of various iterations for each
 * updater thread. It is also responsible for logging the statistics summary when the 
 * updater thread complete.
 * @author Akshay More
 *
 */
public class ProgramStatistics {

	private static Logger logger = Logger.getLogger(ProgramStatistics.class.getName());
	
	private Map<String, UpdaterStatistics> updaterStatistics;
	
	public ProgramStatistics() {
		this.updaterStatistics = new ConcurrentHashMap<String, UpdaterStatistics>();
	}
	
	public void addIterationAttemptStatistics(String updaterName, int iterationNumber,
			int iterationAttemptNumber, long startNanos, long endNanos) {

		if (!updaterStatistics.containsKey(updaterName))
			updaterStatistics.put(updaterName, new UpdaterStatistics(updaterName));

		updaterStatistics.get(updaterName).addIterationAttemptStatistics(
				iterationNumber, iterationAttemptNumber, startNanos, endNanos);
	}
	
	public void logStatistics() {
		logger.info("====================================================================================================");
		for (UpdaterStatistics updaterStats : updaterStatistics.values()) {
			logger.info(updaterStats);
			logger.info("Number of iterations run for thread " + updaterStats.getUpdaterName() 
					+ " = " + updaterStats.getNumberOfIterations());
			
			IterationStatistics slowestIteration = updaterStats.getSlowestIteration();
			IterationStatistics fastestIteration = updaterStats.getFastestIteration();
			if (fastestIteration != null)
				logger.info("Time for fastest iteration = " + fastestIteration.getTotalRunTimeForIteration()/1000000 + " milliseconds.");
			if (slowestIteration != null)
				logger.info("Time for slowest iteration = " + slowestIteration.getTotalRunTimeForIteration()/1000000 + " milliseconds.");
			
			logger.info("Total number of attempts to complete thread " + updaterStats.getUpdaterName() 
					+ " = " + updaterStats.getTotalNumberOfIterationAttempts() + " with min = " 
					+ updaterStats.getMinNumberOfAttempts() + " and max = " + updaterStats.getMaxNumberOfAttempts());
			logger.info("Average number of attempts to complete an iteration for thread " + updaterStats.getUpdaterName() 
					+ " = " + updaterStats.getAverageNumberOfAttemptsToCompleteIteration());
		}
		logger.info("====================================================================================================");
	}
	
}
