package com.glint.codechallenge.statistics;

/**
 * An instance of this class keeps track of the time at which an attempt to complete
 * a grid update iteration stated and ended.
 * @author Akshay More
 *
 */
public final class IterationAttemptStatistics {

	private final int attemptNumber;
	private final long startNanos;
	private final long endNanos;
	
	public IterationAttemptStatistics(int attemptNumber, long startNanos, long endNanos){
		this.attemptNumber = attemptNumber;
		this.startNanos = startNanos;
		this.endNanos = endNanos;
	}

	@Override
	public String toString() {
		if (startNanos > endNanos)
			return "Attempt <" + attemptNumber + "> is in progress.";
		else return "Attempt <" + attemptNumber + "> took <" + (endNanos - startNanos) + "> nanoseconds.";
	}

	protected long getDuration() {
		if (startNanos > endNanos) return 0;
		return (endNanos - startNanos);
	}
}
