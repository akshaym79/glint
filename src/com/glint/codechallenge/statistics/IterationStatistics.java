package com.glint.codechallenge.statistics;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An instance of this class keeps track of the attempts it took to complete a single
 * grid update iteration.
 * @author Akshay More
 *
 */
public class IterationStatistics {

	private final int iterationNumber;
	private Map<Integer, IterationAttemptStatistics> iterationAttemptStatistics;
	
	public IterationStatistics(int iterationNumber) {
		this.iterationNumber = iterationNumber;
		this.iterationAttemptStatistics = new ConcurrentHashMap<Integer, IterationAttemptStatistics>();
	}
	
	protected long getTotalRunTimeForIteration() {
		long totalRunTimeForAttempts = 0;
		for (Map.Entry<Integer, IterationAttemptStatistics> entry : iterationAttemptStatistics.entrySet())
			totalRunTimeForAttempts += entry.getValue().getDuration();
		return totalRunTimeForAttempts;
	}
	
	protected long getNumberOfIterationAttempts() {
		return iterationAttemptStatistics.size();
	}
	
	protected long getAverageRunTimeForIteration() {
		if (getNumberOfIterationAttempts() == 0)
			return 0;
		return (getTotalRunTimeForIteration()/getNumberOfIterationAttempts());
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Iteration number <");
		sb.append(iterationNumber);
		sb.append("> took <");
		sb.append(iterationAttemptStatistics.size());
		sb.append("> attempts with an average run time of <");
		sb.append(getAverageRunTimeForIteration()/1000000.0);
		sb.append("> milliseconds.");
		return sb.toString();
	}

	protected void addIterationAttemptStatistics(int iterationAttemptNumber, long startNanos, long endNanos) {
		iterationAttemptStatistics.put(iterationAttemptNumber, new IterationAttemptStatistics(iterationAttemptNumber, startNanos, endNanos));
	}
	
}
