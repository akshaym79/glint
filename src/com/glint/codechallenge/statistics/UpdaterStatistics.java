package com.glint.codechallenge.statistics;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * An instance of this class keeps track of the run time statistics for an updater thread.
 * @author Akshay More
 *
 */
public class UpdaterStatistics {

	private final String updaterName;
	private Map<Integer, IterationStatistics> iterationStatistics;
	
	public UpdaterStatistics(String updaterName) {
		this.updaterName = updaterName;
		this.iterationStatistics = new ConcurrentHashMap<Integer, IterationStatistics>();
	}
	
	protected String getUpdaterName() {
		return updaterName;
	}
	
	protected Map<Integer, IterationStatistics> getIterationStatistics() {
		return iterationStatistics;
	}

	protected long getNumberOfIterations() {
		return iterationStatistics.size();
	}
	
	protected long getTotalRunTimeForUpdater() {
		long totalRunTimeForUpdater = 0;
		for (Map.Entry<Integer, IterationStatistics> entry : iterationStatistics.entrySet())
			totalRunTimeForUpdater += entry.getValue().getTotalRunTimeForIteration();
		return totalRunTimeForUpdater;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Thread <");
		sb.append(updaterName);
		sb.append("> took a total of <");
		sb.append(getTotalRunTimeForUpdater()/1000000.0);
		sb.append("> milliseconds.");
		return sb.toString();
	}

	protected void addIterationAttemptStatistics(int iterationNumber, int iterationAttemptNumber, long startNanos, long endNanos) {
		if (!iterationStatistics.containsKey(iterationNumber))
			iterationStatistics.put(iterationNumber, new IterationStatistics(iterationNumber));
		
		iterationStatistics.get(iterationNumber).addIterationAttemptStatistics(iterationAttemptNumber, startNanos, endNanos);
	}

	protected IterationStatistics getFastestIteration() {
		if (iterationStatistics.isEmpty()) return null;
		IterationStatistics fastestIteration = null;
		for (IterationStatistics iterStats : iterationStatistics.values()) {
			if (fastestIteration == null)
				fastestIteration = iterStats;
			else {
				if (iterStats.getTotalRunTimeForIteration() < fastestIteration.getTotalRunTimeForIteration())
					fastestIteration = iterStats;
			}
		}
		return fastestIteration;
	}

	protected IterationStatistics getSlowestIteration() {
		if (iterationStatistics.isEmpty()) return null;
		IterationStatistics slowestIteration = null;
		for (IterationStatistics iterStats : iterationStatistics.values()) {
			if (slowestIteration == null)
				slowestIteration = iterStats;
			else {
				if (slowestIteration.getTotalRunTimeForIteration() < iterStats.getTotalRunTimeForIteration())
					slowestIteration = iterStats;
			}
		}
		return slowestIteration;
	}

	protected double getAverageNumberOfAttemptsToCompleteIteration() {
		long totalNumberOfIterations = getNumberOfIterations();
		if (totalNumberOfIterations == 0)
			return 0.0;

		long totalNumberOfAttempts = getTotalNumberOfIterationAttempts();
		return ((double)totalNumberOfAttempts/(double)totalNumberOfIterations); 
	}
	
	protected long getTotalNumberOfIterationAttempts() {
		long totalNumberOfIterationAttempts = 0;
		for (IterationStatistics iterStats : iterationStatistics.values()) {
			totalNumberOfIterationAttempts += iterStats.getNumberOfIterationAttempts();
		}
		return totalNumberOfIterationAttempts;
	}

	protected long getMinNumberOfAttempts() {
		if (iterationStatistics.isEmpty()) return 0;
		long minNumberOfAttempts = Long.MAX_VALUE;
		for (IterationStatistics iterStats : iterationStatistics.values()) {
			if (iterStats.getNumberOfIterationAttempts() < minNumberOfAttempts)
				minNumberOfAttempts = iterStats.getNumberOfIterationAttempts();
		}
		return minNumberOfAttempts;
	}
	
	protected long getMaxNumberOfAttempts() {
		if (iterationStatistics.isEmpty()) return 0;
		long maxNumberOfAttempts = 0;
		for (IterationStatistics iterStats : iterationStatistics.values()) {
			if (maxNumberOfAttempts < iterStats.getNumberOfIterationAttempts())
				maxNumberOfAttempts = iterStats.getNumberOfIterationAttempts();
		}
		return maxNumberOfAttempts;
	}

}
