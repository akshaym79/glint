package com.glint.codechallenge.services;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.glint.codechallenge.exceptions.GridParametersException;
import com.glint.codechallenge.persistence.Cell;
import com.glint.codechallenge.persistence.Grid;
import com.glint.codechallenge.persistence.HibernateUtil;
import com.glint.codechallenge.persistence.IterationState;
import com.glint.codechallenge.util.CellCoordinates;

/**
 * This service manages data persistence and transactions.
 * @author Akshay More
 *
 */
public class PersistenceService {

	private static Logger logger = Logger.getLogger(PersistenceService.class.getName());
	
	public PersistenceService() {
	}
	
	public Grid findGridById(int gridId) {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			@SuppressWarnings("unchecked")
			List<Grid> grids = session.createQuery("from Grid g where g.gridId = " + gridId).list();
			if (grids.isEmpty())
				throw new IllegalArgumentException("Grid for id <" + gridId + "> does not exist.");
			return grids.get(0);
		} finally {
			session.close();
		}
	}
	
	public IterationState getIterationStateById(int iterationStateId) {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			@SuppressWarnings("unchecked")
			List<IterationState> iterationStates = session.createQuery("from IterationState i where i.iterationStateId = " + iterationStateId).list();
			if (iterationStates.isEmpty())
				throw new IllegalArgumentException("IterationState for id <" + iterationStateId + "> does not exist.");
			return iterationStates.get(0);
		} finally {
			session.close();
		}
	}

	public Collection<Cell> getCellsByGridId(int gridId) {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			@SuppressWarnings("unchecked")
			List<Cell> cells = session.createQuery("from Cell c where c.gridId = " + gridId).list();
			if (cells.isEmpty()) 
				throw new IllegalArgumentException("No cells exist for gridId <" + gridId + ">");
			return cells;
		} finally {
			session.close();
		}
	}
	
	public List<IterationState> getIterationStatesByGridId(int gridId) {
		Session session = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			@SuppressWarnings("unchecked")
			List<IterationState> iterationState = session.createQuery("from IterationState i where i.gridId = " + gridId).list();
			return iterationState;
		} finally {
			session.close();
		}
	}

	public IterationState getOrCreateIterationStateByGridIdAndUpdaterName(int gridId, String updaterName) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.setFlushMode(FlushMode.ALWAYS);
			
			@SuppressWarnings("unchecked")
			List<IterationState> iterationStates = session.createQuery(
					"from IterationState i where i.gridId = " + gridId 
					+ " and i.updater = '" + updaterName + "'").list();
			if (iterationStates.isEmpty()) {
				transaction = session.beginTransaction();
				IterationState iterationState = new IterationState(gridId, updaterName);
				session.save(iterationState);
				transaction.commit();
				return iterationState;
			} else {
				return iterationStates.get(0);
			}
		} catch (RuntimeException re1) {
			logger.debug("Runtime exception is: " + re1.getMessage());
			try {
				transaction.rollback();
			} catch (RuntimeException re2) {
				logger.error("Could not roll back transaction to create or get iteration state.", re2);
			}
			throw re1;
		} finally {
			session.close();
		}
	}
	
	public Grid createNewGrid(int N, int M, int K) {
		Session session = null;
		Transaction transaction = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			Grid newGrid = new Grid(N, M, K);
			session.save(newGrid);

			for (int i=0; i<N; i++) {
				for (int j=0; j<N; j++) {
					Cell newCell = (new Cell(newGrid.getGridId(), i, j, 0));
					session.save(newCell);
				}
			}
			
			transaction.commit();
			return newGrid;

		} catch (GridParametersException | RuntimeException e) {
			try {
				transaction.rollback();
				logger.warn("Rolled back transaction to create new grid." + e.getMessage());
				return null;
			} catch (RuntimeException re) {
				logger.error("Could not roll back transaction to create new grid.", re);
				return null;
			}
		} finally {
			session.close();
		}
	}

	public boolean executeIteration(int gridId, Collection<CellCoordinates> cellCoordinatesToUpdate,
			long incrementValue, int iterationStateId) {
		Session session = null;
		Transaction transaction = null;
		IterationState iterationStateToUpdate = null;

		try {
			session = HibernateUtil.getSessionFactory().openSession();

			StringBuffer query = new StringBuffer();
			query.append("from Cell c where c.gridId = " + gridId + " and (");
			for (CellCoordinates cellCoordinates : cellCoordinatesToUpdate) {
				query.append("(c.xCoord = " + cellCoordinates.getxCoord() + " and c.yCoord = " + cellCoordinates.getyCoord() + ") or ");
			}
			query.delete(query.length()-4, query.length());
			query.append(")");
			@SuppressWarnings("unchecked")
			Collection<Cell> cellsToUpdate = session.createQuery(query.toString()).list();
			
			@SuppressWarnings("unchecked")
			List<IterationState> iterationStates = session.createQuery("from IterationState i where i.iterationStateId = " + iterationStateId).list();
			if (iterationStates.isEmpty())
				throw new IllegalArgumentException("IterationState for id <" + iterationStateId + "> does not exist.");
			iterationStateToUpdate =  iterationStates.get(0);

			transaction = session.beginTransaction();
			
			for (Cell cell : cellsToUpdate) {
				cell.incrementValue(incrementValue);
				session.update(cell);
			}
			
			iterationStateToUpdate.incrementCompletedIterations();
			session.update(iterationStateToUpdate);

			transaction.commit();
			logger.debug("Completed iteration " + iterationStateToUpdate.toString());
			return true;

		} catch (RuntimeException re1) {
			try {
				transaction.rollback();
				logger.debug("Rolled back iteration <" + iterationStateToUpdate.getCompletedIterations() + "> for thread <" + iterationStateToUpdate.getUpdater() + "> to update cells.");
			} catch (RuntimeException re2) {
				logger.error("Could not roll back transaction for thread <" + iterationStateToUpdate.getUpdater() + "> to update cells.", re2);
			}
		} finally {
			session.close();
		}
		return false;
	}

}
