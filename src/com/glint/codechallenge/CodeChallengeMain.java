package com.glint.codechallenge;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.glint.codechallenge.exceptions.GridIntegrityException;
import com.glint.codechallenge.persistence.Grid;
import com.glint.codechallenge.persistence.HibernateUtil;
import com.glint.codechallenge.services.PersistenceService;
import com.glint.codechallenge.statistics.ProgramStatistics;

/**
 * This class contains the entry point of the program.
 * @author Akshay More
 *
 */
public class CodeChallengeMain {
	
	private static Logger logger = Logger.getLogger(CodeChallengeMain.class.getName()); 

	@Parameter(names = "-N", description = "Size of the grid")
	private int N = -1;
	
	@Parameter(names = "-M", description = "Number of cells to update per iteration")
	private int M = -1;
	
	@Parameter(names = "-K", description = "Number of iterations")
	private int K = -1;
	
	@Parameter(names = "-recover", description = "Start in recover mode")
	private boolean recoverMode = false;
	
	@Parameter(names = "-gridId", description = "Grid id to recover")
	private int gridId = -1;
	
	@Parameter(names = "-nocrash", description = "Do not start disruptor thread")
	private boolean noCrash = false;
	
	public static void main(String[] args) {
		CodeChallengeMain ccMain = new CodeChallengeMain();
		new JCommander(ccMain, args);
		
		@SuppressWarnings("resource")
		ApplicationContext appCtx = new ClassPathXmlApplicationContext("springConfiguration.xml");
		PersistenceService persistenceService = (PersistenceService)appCtx.getBean("persistenceService");
		ProgramStatistics programStatistics = (ProgramStatistics)appCtx.getBean("programStatistics");
		
		try {
			if (ccMain.recoverMode) {
				if (ccMain.N > 0 && ccMain.M > 0 && ccMain.K > 0) {
					logger.warn("Starting in recover mode. Values of N,M,K will be ignored.");
				}
				if (ccMain.gridId > 0) {
					logger.info("Starting in recover mode for gridId <" + ccMain.gridId + ">");
					Grid existingGrid = persistenceService.findGridById(ccMain.gridId);
					if (existingGrid != null) {
						logger.info("Found exising grid " + existingGrid.toString());
						new GridRunner(existingGrid, persistenceService, programStatistics).run(ccMain.noCrash);
					}
				} else {
					logger.error("Must provide gridId when starting in recover mode.");
				}
			} else {
				if (ccMain.gridId > 0) {
					logger.warn("Starting in new grid mode. Value of gridId will be ignored.");
				}
				if (ccMain.N > 0 && ccMain.M > 0 && ccMain.K > 0) {
					logger.info("Starting new grid with N,M,K = <" + ccMain.N + "," + ccMain.M + "," + ccMain.K + ">");
					
					Grid newGrid = persistenceService.createNewGrid(ccMain.N, ccMain.M, ccMain.K);
					if (newGrid != null) {
						logger.info("Created a new grid " + newGrid.toString());
						new GridRunner(newGrid, persistenceService, programStatistics).run(ccMain.noCrash);
					} else {
						logger.info("Failed to create a new grid. Exiting!");
					}
				} else {
					logger.error("Must provide values for N,M,K when starting a new grid.");
				}
			}
		} catch (GridIntegrityException gie) {
			logger.error(gie.getMessage(), gie);
		} finally {
			HibernateUtil.shutdown();
		}
	}

}
