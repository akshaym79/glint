package com.glint.codechallenge;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.glint.codechallenge.exceptions.GridIntegrityException;
import com.glint.codechallenge.persistence.Cell;
import com.glint.codechallenge.persistence.Grid;
import com.glint.codechallenge.persistence.IterationState;
import com.glint.codechallenge.services.PersistenceService;
import com.glint.codechallenge.statistics.ProgramStatistics;

import java.lang.Runnable;

import org.apache.log4j.Logger;

/**
 * An instance of this class controls the flow of the program by creating the two threads A and B that 
 * update the cells of the grid and also the thread C (if -nocrash if not specified) that crashes the
 * program.
 * It also contains logic to validate the invariant of the grid that the sum of all cell values is 
 * some multiples of M * K * 10 plus some other multiples of M.
 * @author Akshay More
 *
 */
public class GridRunner {
	
	private static Logger logger = Logger.getLogger(GridRunner.class.getName());

	private final Grid grid;
	private PersistenceService persistenceService;
	private ProgramStatistics programStatistics;
	
	public GridRunner(Grid grid, PersistenceService persistenceService, ProgramStatistics programStatistics) {
		this.grid = grid;
		this.persistenceService = persistenceService;
		this.programStatistics = programStatistics;
	}
	
	public void run(boolean noCrash) throws GridIntegrityException {
		Collection<Cell> cells = persistenceService.getCellsByGridId(grid.getGridId());

		// Check the data integrity of the grid.
		checkGridIntegrity(cells);
		
		Runnable threadA = new Updater("A", 1, grid, new Random(new Object().hashCode()), persistenceService, programStatistics);
		Runnable threadB = new Updater("B", grid.getK()*10, grid, new Random(new Object().hashCode()), persistenceService, programStatistics);
		
		ExecutorService executorService = Executors.newFixedThreadPool(3);
		CompletionService<String> completionService = new ExecutorCompletionService<String>(executorService);
		
		completionService.submit(threadA, "A");
		completionService.submit(threadB, "B");

		// Start the disruptor thread if needed.
		if (!noCrash) {
			Runnable threadC = new Disruptor("C", grid, persistenceService);
			executorService.submit(threadC);
		}
		
		// Wait for the thread A and B to finish.
		try {
			logger.debug("GridRunner is done with thread <" + completionService.take().get() + ">");
		} catch (InterruptedException e) {
			logger.error("Interrupted while waiting for thread completion." + e.getMessage());
		} catch (ExecutionException e) {
			logger.error("Execution error occurred while waiting for thread completion." + e.getMessage());
		}
		try {
			logger.debug("GridRunner is done with thread <" + completionService.take().get() + ">");
		} catch (InterruptedException e) {
			logger.error("Interrupted while waiting for thread completion." + e.getMessage());
		} catch (ExecutionException e) {
			logger.error("Execution error occurred while waiting for thread completion." + e.getMessage());
		}
		executorService.shutdown();
		programStatistics.logStatistics();
	}

	/**
	 * Method to check the data integrity of the grid cells.
	 * @param cells Cells in the grid.
	 * @throws GridIntegrityException If the data integrity of the grid is lost.
	 */
	private void checkGridIntegrity(Collection<Cell> cells) throws GridIntegrityException {
		int contributionByA = 0;
		int contributionByB = 0;
		int sumOfCellValues = 0;
		for (Cell cell : cells)
			sumOfCellValues += cell.getValue();
		
		List<IterationState> iterationStates = persistenceService.getIterationStatesByGridId(grid.getGridId());
		if (iterationStates == null)
			throw new GridIntegrityException("List of iteration states should not be null.");
		logger.debug("Found <" + iterationStates.size() + "> iteration states for gridId <" + grid.getGridId() + ">");
		for (IterationState is : iterationStates)
			logger.debug(is);

		if (iterationStates.size() > 2) {
			logger.error("More than two iteration states found for gridId <" + grid.getGridId() + ">");
			throw new GridIntegrityException("There are more than 2 iteration states for gridId <" + grid.getGridId() + ">. This should never happen.");
		}

		if (iterationStates.isEmpty()) {
			for (Cell cell : cells)
				if (cell.getValue() != 0)
					throw new GridIntegrityException("Sum of cells for gridId <" + grid.getGridId() + "> is expected to be zero");
		}  
		
		if (iterationStates.size() == 1) {
			if (iterationStates.get(0).getUpdater().equals("A")) {
				contributionByA = iterationStates.get(0).getCompletedIterations() * grid.getM() * 1;
			} else {
				contributionByB = iterationStates.get(0).getCompletedIterations() * grid.getM() * grid.getK() * 10;
			}
		}
		
		if (iterationStates.size() == 2) {
			if (iterationStates.get(0).getUpdater().equals("A")) {
				contributionByA = iterationStates.get(0).getCompletedIterations() * grid.getM() * 1;
				contributionByB = iterationStates.get(1).getCompletedIterations() * grid.getM() * grid.getK() * 10;
			} else {
				contributionByA = iterationStates.get(1).getCompletedIterations() * grid.getM() * 1;
				contributionByB = iterationStates.get(0).getCompletedIterations() * grid.getM() * grid.getK() * 10;
			}
		}

		logger.debug("Total contribution from threads A and B is (" 
				+ contributionByA + " + " + contributionByB + ") = " + (contributionByA + contributionByB));
		logger.debug("Sum of all cell values = " + sumOfCellValues);
		if ((contributionByA + contributionByB) != sumOfCellValues) {
			throw new GridIntegrityException("Total contribution from threads A and B (" 
				+ contributionByA + " + " + contributionByB + ") = " + (contributionByA + contributionByB) 
				+ " does not match Sum of all cell values = " + sumOfCellValues);
		} else {
			logger.debug("The grid <" + grid.getGridId() + "> has passed integrity check. Safe to proceed ...");
		}
	}
}
