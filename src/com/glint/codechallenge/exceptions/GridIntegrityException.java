package com.glint.codechallenge.exceptions;

/**
 * An instance of this class represents an exception that the data integrity of the grid is lost.
 * @author Akshay More
 *
 */
public class GridIntegrityException extends Exception {
	private static final long serialVersionUID = 5500568468981187414L;

	public GridIntegrityException() {
		super();
	}

	public GridIntegrityException(String message, Throwable t) {
		super(message, t);
	}

	public GridIntegrityException(String message) {
		super(message);
	}

	public GridIntegrityException(Throwable t) {
		super(t);
	}
	
}
