package com.glint.codechallenge.exceptions;

/**
 * An instance of this class represents the exception that the grid parameters are invalid.
 * @author Akshay More
 *
 */
public class GridParametersException extends Exception {

	private static final long serialVersionUID = 1056889429209895613L;

	public GridParametersException() {
		super();
	}

	public GridParametersException(String message, Throwable t) {
		super(message, t);
	}

	public GridParametersException(String message) {
		super(message);
	}

	public GridParametersException(Throwable t) {
		super(t);
	}

}
