package com.glint.codechallenge;

import java.util.List;

import org.apache.log4j.Logger;

import com.glint.codechallenge.persistence.Grid;
import com.glint.codechallenge.persistence.IterationState;
import com.glint.codechallenge.services.PersistenceService;

/**
 * An instance of this class is a disruptor thread that monitors the number of iterations completed
 * and crashes the program by exiting the JVM if the number of completed iterations is between
 * 40% and 60% of the total number of iterations.
 * The thread checks for this condition every 1000 milliseconds.
 * @author Akshay More
 *
 */
public class Disruptor implements Runnable {

	private static Logger logger = Logger.getLogger(Disruptor.class.getName());
	
	private String name;
	private Grid grid;
	private PersistenceService persistenceService;
	
	public Disruptor(String name, Grid grid, PersistenceService persistenceService) {
		this.name = name;
		this.grid = grid;
		this.persistenceService = persistenceService;
	}
	
	public void run() {
		logger.info("Starting disruptor thread " + name + " on gridId <" + grid.getGridId() + ">");
		int iterationsLowerLimit = (int)Math.floor(grid.getK() * 0.4);
		int iterationsUpperLimit = (int)Math.ceil(grid.getK() * 0.6);
		int totalCompletedIterations = 0;

		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.warn("Disruptor thread " + name + " was interrupted, but that's okay!");
			}
			List<IterationState> iterationStates = persistenceService.getIterationStatesByGridId(grid.getGridId());
			totalCompletedIterations = 0;
			for (IterationState is : iterationStates)
				totalCompletedIterations += is.getCompletedIterations();
			logger.info("Total completed iterations = <" + totalCompletedIterations + ">, disruption range is <" + iterationsLowerLimit + "> to <" + iterationsUpperLimit + ">");
			if (iterationsLowerLimit <= totalCompletedIterations && totalCompletedIterations <= iterationsUpperLimit) {
				logger.info("Total completed iterations <" + totalCompletedIterations + "> is within the range of <" + iterationsLowerLimit + "> and <" + iterationsUpperLimit + ">");
				logger.info("The program will self destruct in 3 ... 2 ... 1 ... BOOM!");
				System.exit(0);
			} else if (totalCompletedIterations < iterationsLowerLimit) {
				logger.info("Disruptor thread " + name + " must be patient.");
			} else {
				logger.info("The program is safe from disruptor thread " + name);
			}
		}
	}

}
