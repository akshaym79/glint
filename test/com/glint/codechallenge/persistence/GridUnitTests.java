package com.glint.codechallenge.persistence;

import static org.junit.Assert.*;
import org.junit.Test;
import com.glint.codechallenge.exceptions.GridParametersException;

/**
 * Some basic unit tests for creation of the Grid object.
 * @author Akshay More
 *
 */
public class GridUnitTests {

	@Test
	public void testGridParameters() {
	
		try {
			new Grid(1, 2, 10);
			fail("Grid creation with N=1 and M=2 should throw an exception.");
		} catch (GridParametersException e) {
			assertTrue("Exception message is as expected.", e.getMessage().contains("Value of M should not be greater N^2"));
		}
		
		try {
			new Grid(-1, 2, 10);
			fail("Grid creation with negative parameter values should throw an exception.");
		} catch (GridParametersException e) {
			assertTrue("Exception message is as expected.", e.getMessage().contains("Values of N,K,M should not be negative"));
		}
		
		try {
			Grid g = new Grid(2, 1, 10);
			assertTrue(g.getN() == 2);
			assertTrue(g.getM() == 1);
			assertTrue(g.getK() == 10);
		} catch (Exception e) {
			fail("Creation of grid with valid parameters is not expected to fail.");
		}
	}
}
