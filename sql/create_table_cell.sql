 CREATE TABLE `glint`.`cell` (
  `cellId` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `version` BIGINT(20) NULL COMMENT '',
  `gridId` INT NULL COMMENT '',
  `xCoord` INT NULL COMMENT '',
  `yCoord` INT NULL COMMENT '',
  `value` BIGINT(20) NULL COMMENT '',
  PRIMARY KEY (`cellId`)  COMMENT '',
  UNIQUE INDEX `cellId_UNIQUE` (`cellId` ASC)  COMMENT '');
  
ALTER TABLE `glint`.`cell` 
ADD INDEX `IDX_gridId_coords` (`gridId` ASC, `xCoord` ASC, `yCoord` ASC)  COMMENT '';
  