CREATE TABLE `glint`.`iterationstate` (
  `iterationStateId` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
  `version` BIGINT(20) NULL COMMENT '',
  `gridId` INT NULL COMMENT '',
  `updater` VARCHAR(45) NULL COMMENT '',
  `completedIterations` INT NULL COMMENT '',
  PRIMARY KEY (`iterationStateId`)  COMMENT '',
  UNIQUE INDEX `iterationStateId_UNIQUE` (`iterationStateId` ASC)  COMMENT '');
  
ALTER TABLE `glint`.`iterationstate` 
ADD INDEX `IDX_gridId` (`gridId` ASC)  COMMENT '';
  